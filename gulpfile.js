var gulp = require('gulp');
var sync = require('gulp-npm-script-sync');

var webpack = require('webpack-stream');
var htmlreplace = require('gulp-html-replace');
var inject = require('gulp-inject');

var jshint = require('gulp-jshint');
var less = require('gulp-less-to-scss');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var HTMLmin = require('gulp-htmlmin');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var es = require('event-stream');

// JavaScript linting task
gulp.task('jshint', function() {
	return gulp.src(['js/main.js', 'js/app.js'])
	.pipe(jshint())
	.pipe(jshint.reporter('default'));
});

// Convert Less to Sass
gulp.task('less', function() {
	return gulp.src('css/less/*.less')
	.pipe(less())
	.pipe(gulp.dest('css/scss'));
});

// Compile Sass task
gulp.task('sass', function() {
 	return gulp.src('css/scss/*.scss')
 	.pipe(sass())
	.pipe(gulp.dest('css'));
});

// Watch task
gulp.task('watch', function() {
	gulp.watch(['js/*.js'], ['jshint']);
	gulp.watch('scss/*.scss', ['sass']);
});

// Default task
gulp.task('default', ['jshint', 'sass', 'watch']);

// HTML task
gulp.task('html', ['index', 'mini']);

// Minify index
gulp.task('mini', function() {
	return gulp.src('./index.html')
	.pipe(htmlreplace({
		'css': 'build/css/portfolio.min.css',
		'js': 'build/js/portfolio.min.js'
	}))
	.pipe(HTMLmin({collapseWhitespace: true}))
	.pipe(rename(function(path) {
		path.dirname += './build';
		path.basename += '-min';
		path.extname = '.html'
	}))
	.pipe(gulp.dest('build/'));
});

gulp.task('index', function() {
	return gulp.src('./index.html')
	.pipe(htmlreplace({
		'css': 'build/css/portfolio.css',
		'js': 'build/js/portfolio.js'
	}))
	.pipe(gulp.dest('build/'));
});

// Inject .js files
gulp.task('inject', ['jquery', 'ga']);

gulp.task('jquery', function() {
	gulp.src('index.html')
	.pipe(inject(gulp.src('node_modules/jquery/dist/jquery.js'), {
		starttag: '<!-- inject:jquery -->',
		transform: function (filePath, file) {
			return file.contents.toString('utf8')
		}
	}))
	.pipe(gulp.dest('build/'));
});

gulp.task('ga', function() {
	gulp.src('index.html')
	.pipe(inject(gulp.src('js/analytics.js'), {
		starttag: '<!-- inject:analytics -->',
		transform: function (filePath, file) {
			return file.contents.toString('utf8')
		}
	}))
	.pipe(gulp.dest('build/'));
});

// JavaScript build task, removes whitespace and concatenates all files
gulp.task('js', function() {
	var home = browserify(['js/main.js', 'js/app.js'])
	.bundle()
	.pipe(source('scripts.js'))
	.pipe(gulp.dest('js'));

	var work = browserify(['js/main.js', 'js/app.js'])
	.bundle()
	.pipe(source('scripts.js'))
	.pipe(buffer())
	.pipe(uglify())
	.pipe(gulp.dest('build/js'));

	return es.concat(home, work);
});

// Scripts build task
gulp.task('scripts', ['js', 'inject']);

// Styles build task, concatenates all files
gulp.task('css', function() {
	return gulp.src(['css/main.css', 'css/normalize.css'])
	.pipe(concat('styles.css'))
	.pipe(cssnano())
	.pipe(gulp.dest('build/css'));
});

// CSS, LESS, and SASS build task
gulp.task('styles', ['less', 'sass', 'css']);

// Image optimization task
gulp.task('images', ['img', 'ico']);

gulp.task('img', function() {
	return gulp.src('images/*')
    .pipe(imagemin())
		.pipe(gulp.dest('build/images'));
});

gulp.task('ico', function() {
	return gulp.src('images/icons/*')
		.pipe(imagemin())
		.pipe(gulp.dest('build/images/icons'));
});

// gulp.task('webpack', function() {
// 	return gulp.src('js/*.js')
// 		.pipe(webpack(require('./webpack.config.js')))
// 		.pipe(gulp.dest('build/js'));
// });

// Build task
gulp.task('build:app', ['jshint', 'html', 'scripts', 'styles', 'images']);

gulp.task('sync', function() {
	sync(gulp, {
		path: './package.json'
	});
});
