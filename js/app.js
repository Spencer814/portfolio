var $ = require('jquery');

$(document).ready(function(){

  $('.home').click(function(){
    $("#home").show();
    $("#projects").hide();
    $("#blog").hide();
  });

  $('.projects').click(function(){
    $("#projects").show();
    $("#home").hide();
    $("#blog").hide();
  });

  $('.blog').click(function(){
    $("#blog").show();
    $("#home").hide();
    $("#projects").hide();
  });

  $('#home').removeClass('hidden');

});
